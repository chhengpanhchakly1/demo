import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  errors: any = [];

  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(login: any) {
    console.log('Form Submit', login)
  }

}
